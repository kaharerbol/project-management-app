from django.shortcuts import render, redirect
from tasks.forms import TaskForm, TaskDetailForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "task/create_task.html", context)


@login_required
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "task": task,
    }
    return render(request, "task/show_my_task.html", context)


@login_required
def task_detail(request, id):
    task = Task.objects.get(id=id)
    context = {
        "task_detail": task,
    }
    return render(request, "task/task_detail.html", context)


@login_required
def task_log(request):
    if request.method == "POST":
        form = TaskDetailForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskDetailForm()
    context = {
        "form": form,
    }
    return render(request, "task/task_log.html", context)
