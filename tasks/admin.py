from django.contrib import admin
from tasks.models import Task, TaskLog


# Register your models here.
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )


@admin.register(TaskLog)
class TaskLogAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "log",
        "task",
        "log_date",
    )
