from django.urls import path
from tasks.views import create_task, show_my_tasks, task_detail, task_log


urlpatterns = [
    path("log/", task_log, name="task_log"),
    path("<int:id>/", task_detail, name="task_detail"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
