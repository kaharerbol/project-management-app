from django.forms import ModelForm
from tasks.models import Task, TaskLog


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )


class TaskDetailForm(ModelForm):
    class Meta:
        model = TaskLog
        fields = (
            "log",
            "task",
            "log_date",
        )
